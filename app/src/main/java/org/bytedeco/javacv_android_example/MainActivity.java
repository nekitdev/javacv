package org.bytedeco.javacv_android_example;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.getkeepsafe.relinker.ReLinker;

import org.bytedeco.javacpp.Loader;
import org.bytedeco.javacv_android_example.record.RecordActivity;
import org.bytedeco.opencv.global.opencv_core;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewById(R.id.btnRecord).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, RecordActivity.class));
            }
        });

        Loader.load(opencv_core.class);
    }
}
